use super::theme_chooser::ThemeChooser;
use crate::app::{Action, App};
use crate::article_view::ArticleTheme;
use crate::settings::article_list::GArticleOrder;
use crate::settings::feed_list::GFeedOrder;
use crate::util::{constants, Util};
use glib::{clone, subclass, RustClosure};
use gtk4::ClosureExpression;
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, FontButton, GestureClick, Inhibit, Label, SpinButton, Switch,
    Widget,
};
use libadwaita::{prelude::*, subclass::prelude::*, ActionRow, ComboRow, PreferencesPage};
use libadwaita::{EnumListItem, EnumListModel};
use news_flash::models::ArticleOrder;
use parking_lot::RwLock;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/settings_views.ui")]
    pub struct SettingsViewsPage {
        #[template_child]
        pub feed_order_row: TemplateChild<ComboRow>,
        #[template_child]
        pub filter_feeds_switch: TemplateChild<Switch>,
        #[template_child]
        pub article_order_row: TemplateChild<ComboRow>,
        #[template_child]
        pub show_thumbs_switch: TemplateChild<Switch>,
        #[template_child]
        pub hide_future_switch: TemplateChild<Switch>,
        #[template_child]
        pub article_theme_label: TemplateChild<Label>,
        #[template_child]
        pub article_theme_click: TemplateChild<GestureClick>,
        #[template_child]
        pub allow_selection_switch: TemplateChild<Switch>,
        #[template_child]
        pub content_width_spin_button: TemplateChild<SpinButton>,
        #[template_child]
        pub line_height_spin_button: TemplateChild<SpinButton>,
        #[template_child]
        pub font_row: TemplateChild<ActionRow>,
        #[template_child]
        pub font_button: TemplateChild<FontButton>,
        #[template_child]
        pub use_system_font_switch: TemplateChild<Switch>,

        pub article_theme_popover: RwLock<Option<ThemeChooser>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsViewsPage {
        const NAME: &'static str = "SettingsViewsPage";
        type ParentType = PreferencesPage;
        type Type = super::SettingsViewsPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsViewsPage {}

    impl WidgetImpl for SettingsViewsPage {}

    impl PreferencesPageImpl for SettingsViewsPage {}
}

glib::wrapper! {
    pub struct SettingsViewsPage(ObjectSubclass<imp::SettingsViewsPage>)
        @extends Widget, PreferencesPage;
}

impl Default for SettingsViewsPage {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SettingsViewsPage {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self) {
        let imp = self.imp();

        // -----------------------------------------------------------------------
        // feed list order
        // -----------------------------------------------------------------------
        let params: &[gtk4::Expression] = &[];
        let closure = RustClosure::new(|values| {
            let e = values[0].get::<EnumListItem>().unwrap();
            let feed_order = GFeedOrder::from(e.value());
            let order = feed_order.to_string().to_value();
            Some(order)
        });
        let feed_order_closure = ClosureExpression::new::<String>(params, closure);
        imp.feed_order_row.set_expression(Some(&feed_order_closure));
        imp.feed_order_row
            .set_model(Some(&EnumListModel::new(GFeedOrder::static_type())));
        imp.feed_order_row
            .set_selected(GFeedOrder::from(App::default().settings().read().get_feed_list_order()).into());

        // -----------------------------------------------------------------------
        // feed list show only relevant
        // -----------------------------------------------------------------------
        imp.filter_feeds_switch
            .set_active(App::default().settings().read().get_feed_list_only_show_relevant());

        // -----------------------------------------------------------------------
        // article list order
        // -----------------------------------------------------------------------
        let params: &[gtk4::Expression] = &[];
        let closure = RustClosure::new(|values| {
            let e = values[0].get::<EnumListItem>().unwrap();
            let article_order: GArticleOrder = e.value().into();
            let article_order: ArticleOrder = article_order.into();
            let order = article_order.to_str().to_value();
            Some(order)
        });
        let article_order_closure = ClosureExpression::new::<String>(params, closure);
        imp.article_order_row.set_expression(Some(&article_order_closure));
        imp.article_order_row
            .set_model(Some(&EnumListModel::new(GArticleOrder::static_type())));
        imp.article_order_row
            .set_selected(GArticleOrder::from(App::default().settings().read().get_article_list_order()).into());

        // -----------------------------------------------------------------------
        // article list show thumbnails
        // -----------------------------------------------------------------------
        imp.show_thumbs_switch
            .set_active(App::default().settings().read().get_article_list_show_thumbs());

        // -----------------------------------------------------------------------
        // article list hide future articles
        // -----------------------------------------------------------------------
        imp.hide_future_switch
            .set_active(App::default().settings().read().get_article_list_hide_future_articles());

        // -----------------------------------------------------------------------
        // article view theme
        // -----------------------------------------------------------------------
        imp.article_theme_label
            .set_label(App::default().settings().read().get_article_view_theme().name());
        let article_theme_popover = ThemeChooser::new();
        article_theme_popover.set_parent(&*imp.article_theme_label);
        imp.article_theme_popover.write().replace(article_theme_popover);

        // -----------------------------------------------------------------------
        // article view allow selection
        // -----------------------------------------------------------------------
        imp.allow_selection_switch
            .set_active(App::default().settings().read().get_article_view_allow_select());

        // -----------------------------------------------------------------------
        // article view content width
        // -----------------------------------------------------------------------
        imp.content_width_spin_button.set_value(
            App::default()
                .settings()
                .read()
                .get_article_view_width()
                .unwrap_or(constants::DEFAULT_ARTICLE_CONTENT_WIDTH) as f64,
        );

        // -----------------------------------------------------------------------
        // article view line height
        // -----------------------------------------------------------------------
        imp.line_height_spin_button.set_value(
            App::default()
                .settings()
                .read()
                .get_article_view_line_height()
                .unwrap_or(constants::DEFAULT_ARTICLE_LINE_HEIGHT) as f64,
        );

        // -----------------------------------------------------------------------
        // article view fonts
        // -----------------------------------------------------------------------
        let have_custom_font = App::default().settings().read().get_article_view_font().is_some();
        imp.font_row.set_sensitive(have_custom_font);
        imp.font_button.set_sensitive(have_custom_font);
        if let Some(font) = App::default().settings().read().get_article_view_font() {
            imp.font_button.set_font(font);
        }
        imp.use_system_font_switch.set_active(!have_custom_font);

        self.setup_feed_list();
        self.setup_article_list();
        self.setup_article_view();
    }

    fn theme_pop(&self) -> ThemeChooser {
        let imp = self.imp();
        imp.article_theme_popover
            .read()
            .clone()
            .expect("SettingsViewsPage not initialized")
    }

    fn setup_feed_list(&self) {
        let imp = self.imp();

        imp.feed_order_row.connect_selected_notify(|row| {
            let selected_index = row.selected();
            let new_order: GFeedOrder = selected_index.into();
            if App::default()
                .settings()
                .write()
                .set_feed_list_order(new_order.into())
                .is_ok()
            {
                Util::send(Action::UpdateSidebar);
            } else {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'article order'.".to_owned(),
                ));
            }
        });

        imp.filter_feeds_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_feed_list_only_show_relevant(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'show relevant feeds'.".to_owned(),
                ));
            }
            Util::send(Action::UpdateSidebar);
            Inhibit(false)
        });
    }

    fn setup_article_list(&self) {
        let imp = self.imp();

        imp.article_order_row.connect_selected_notify(|row| {
            let selected_index = row.selected();
            let new_order: GArticleOrder = selected_index.into();
            if App::default()
                .settings()
                .write()
                .set_article_list_order(new_order.into())
                .is_ok()
            {
                Util::send(Action::UpdateArticleList);
            } else {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'article order'.".to_owned(),
                ));
            }
        });

        imp.show_thumbs_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_article_list_show_thumbs(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'show thumbnails'.".to_owned(),
                ));
            }
            Util::send(Action::UpdateArticleList);
            Inhibit(false)
        });

        imp.hide_future_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_article_list_hide_future_articles(is_set)
                .is_err()
            {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'show thumbnails'.".to_owned(),
                ));
            }
            Util::send(Action::UpdateSidebar);
            Util::send(Action::UpdateArticleList);
            Inhibit(false)
        });
    }

    fn setup_article_view(&self) {
        let imp = self.imp();
        let article_theme_popover = self.theme_pop();

        imp.article_theme_click.connect_released(clone!(
            @weak article_theme_popover => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            article_theme_popover.popup();
        }));

        let article_theme_label = imp.article_theme_label.get();
        article_theme_popover.connect_local("article-theme-changed", false, move |val| {
            let article_theme = val[1]
                .get::<ArticleTheme>()
                .expect("The value needs to be of type `ArticleTheme`.");
            if App::default()
                .settings()
                .write()
                .set_article_view_theme(&article_theme)
                .is_err()
            {
                Util::send(Action::SimpleMessage("Failed to set article theme.".into()));
            }
            article_theme_label.set_label(article_theme.name());
            App::default()
                .main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .redraw_article();
            None
        });

        imp.allow_selection_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_article_view_allow_select(is_set)
                .is_ok()
            {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .article_view()
                    .redraw_article();
            } else {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'allow article selection'.".to_owned(),
                ));
            }
            Inhibit(false)
        });

        imp.content_width_spin_button.connect_value_notify(|spin_button| {
            if App::default()
                .settings()
                .write()
                .set_article_view_width(Some(spin_button.value_as_int() as u32))
                .is_ok()
            {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .article_view()
                    .reload_style_sheet();
            } else {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'content width'.".to_owned(),
                ));
            }
        });

        imp.line_height_spin_button.connect_value_notify(|spin_button| {
            if App::default()
                .settings()
                .write()
                .set_article_view_line_height(Some(spin_button.value() as f32))
                .is_ok()
            {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .article_view()
                    .reload_style_sheet();
            } else {
                Util::send(Action::SimpleMessage("Failed to set setting 'line height'.".to_owned()));
            }
        });

        imp.font_button.connect_font_set(|button| {
            let font = button.font().map(|font| font.to_string());
            if App::default().settings().write().set_article_view_font(font).is_ok() {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .article_view()
                    .redraw_article();
            } else {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'article font'.".to_owned(),
                ));
            }
        });

        let font_button = imp.font_button.get();
        let font_row = imp.font_row.get();
        imp.use_system_font_switch.connect_state_set(move |_switch, is_set| {
            let font = if is_set {
                None
            } else {
                font_button.font().map(|font_name| font_name.to_string())
            };
            font_button.set_sensitive(!is_set);
            font_row.set_sensitive(!is_set);
            if App::default().settings().write().set_article_view_font(font).is_ok() {
                App::default()
                    .main_window()
                    .content_page()
                    .articleview_column()
                    .article_view()
                    .redraw_article();
            } else {
                Util::send(Action::SimpleMessage(
                    "Failed to set setting 'use system font'.".to_owned(),
                ));
            }
            Inhibit(false)
        });
    }
}
