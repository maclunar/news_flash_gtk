#![allow(clippy::derived_hash_with_manual_eq)]

mod about_dialog;
mod account_popover;
mod account_widget;
mod add_popover;
mod app;
mod article_list;
mod article_view;
mod color;
mod config;
mod content_page;
mod discover;
mod edit_category_dialog;
mod edit_feed_dialog;
mod edit_tag_dialog;
mod enclosure_popover;
mod error;
mod error_dialog;
mod i18n;
mod login_screen;
mod main_window;
mod reset_page;
mod responsive;
mod self_stack;
mod settings;
mod share;
mod shortcuts_dialog;
mod sidebar;
mod tag_popover;
mod undo_action;
mod util;
mod welcome_screen;

use crate::app::App;
use crate::config::{APP_ID, VERSION};
use gettextrs::*;
use log::LevelFilter;
use log4rs::append::console::ConsoleAppender;
use log4rs::config::{Appender, Config, Logger, Root};
use log4rs::encode::pattern::PatternEncoder;

fn main() {
    // nicer backtrace
    color_backtrace::install();

    let matches = clap::Command::new("NewsFlash")
        .version(VERSION)
        .author("Jan Lukas Gernert <jangernert@gmail.com>")
        .about("Feed Reader written in GTK")
        .arg(
            clap::Arg::new("VERBOSE")
                .short('v')
                .long("verbose")
                .help("Sets the level of logs to debug.")
                .required(false)
                .num_args(0),
        )
        .arg(
            clap::Arg::new("INSPECT")
                .short('i')
                .long("inspect")
                .help("Allow to show webview inspector.")
                .required(false)
                .num_args(0),
        )
        .arg(
            clap::Arg::new("HEADLESS")
                .long("headless")
                .help("Start without showing the window.")
                .required(false)
                .num_args(0),
        )
        .get_matches();

    let log_level = if *matches.get_one::<bool>("VERBOSE").unwrap_or(&false) {
        LevelFilter::Debug
    } else {
        LevelFilter::Info
    };

    let allow_inspector = *matches.get_one::<bool>("INSPECT").unwrap_or(&false);
    let headless = *matches.get_one::<bool>("HEADLESS").unwrap_or(&false);

    // Logging
    let encoder = PatternEncoder::new("{d(%H:%M:%S)} - {h({({l}):5.5})} - {m:<35.} (({M}:{L}))\n");
    let stdout = ConsoleAppender::builder().encoder(Box::new(encoder)).build();
    let appender = Appender::builder().build("stdout", Box::new(stdout));
    let root = Root::builder().appender("stdout").build(log_level);

    let reqwest = Logger::builder().additive(false).build("reqwest", LevelFilter::Off);
    let hyper = Logger::builder().additive(false).build("hyper", LevelFilter::Off);
    let trust_dns_resolver = Logger::builder()
        .additive(false)
        .build("trust_dns_resolver", LevelFilter::Off);
    let trust_dns_proto = Logger::builder()
        .additive(false)
        .build("trust_dns_proto", LevelFilter::Off);

    let config = Config::builder()
        .appender(appender)
        .logger(reqwest)
        .logger(hyper)
        .logger(trust_dns_resolver)
        .logger(trust_dns_proto)
        .build(root)
        .expect("Failed to create log4rs config.");
    let _handle = log4rs::init_config(config).expect("Failed to init log4rs config.");

    // Gtk setup
    gtk4::init().expect("Error initializing gtk.");
    gtk4::Window::set_default_icon_name(APP_ID);
    glib::set_application_name("NewsFlash");
    glib::set_prgname(Some("NewsFlashGTK"));

    // Setup translations
    setlocale(LocaleCategory::LcAll, "");
    let localedir = bindtextdomain(config::PKGNAME, config::LOCALEDIR).unwrap();
    let textdomain = textdomain(config::PKGNAME).unwrap();

    log::debug!("LOCALEDIR: {:?}", localedir);
    if let Ok(textdomain) = std::str::from_utf8(&textdomain) {
        log::debug!("TEXTDOMAIN: {}", textdomain);
    }

    // Run app itself
    App::run(allow_inspector, headless);
}
