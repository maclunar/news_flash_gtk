use ashpd::desktop::settings::Settings;
use gio::Settings as GSettings;
use glib::clone;
use gtk4::prelude::SettingsExt;
use parking_lot::RwLock;
use std::sync::Arc;

pub const DEFAULT_DOCUMENT_FONT: &str = "Cantarell 11";

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum ClockFormat {
    F12H,
    F24H,
}

pub struct DesktopSettings {
    clock_format: Arc<RwLock<ClockFormat>>,
    document_font: Arc<RwLock<String>>,
}

impl Default for DesktopSettings {
    fn default() -> Self {
        Self {
            clock_format: Arc::new(RwLock::new(ClockFormat::F24H)),
            document_font: Arc::new(RwLock::new(DEFAULT_DOCUMENT_FONT.into())),
        }
    }
}

impl DesktopSettings {
    pub fn init(&self) {
        glib::MainContext::default().spawn_local(clone!(
            @strong self.clock_format as clock_format,
            @strong self.document_font as document_font => async move {
            let font = Self::get_document_font().await.unwrap_or(DEFAULT_DOCUMENT_FONT.into());
            let format = Self::get_clock_format().await.unwrap_or(ClockFormat::F24H);

            *document_font.write() = font;
            *clock_format.write() = format;
        }));
    }

    async fn get_document_font() -> ashpd::Result<String> {
        if ashpd::is_sandboxed().await {
            let settings = Settings::new().await?;
            let font = settings
                .read::<String>("org.gnome.desktop.interface", "document-font-name")
                .await?;
            Ok(font)
        } else {
            Ok(GSettings::new("org.gnome.desktop.interface")
                .string("document-font-name")
                .as_str()
                .into())
        }
    }

    async fn get_clock_format() -> ashpd::Result<ClockFormat> {
        let clock_format = if ashpd::is_sandboxed().await {
            let settings = Settings::new().await?;
            settings
                .read::<String>("org.gnome.desktop.interface", "clock-format")
                .await?
        } else {
            GSettings::new("org.gnome.desktop.interface")
                .string("clock-format")
                .as_str()
                .into()
        };

        Ok(match clock_format.as_str() {
            "12h" => ClockFormat::F12H,
            "24h" => ClockFormat::F24H,
            _ => ClockFormat::F24H,
        })
    }

    pub fn clock_format(&self) -> ClockFormat {
        *self.clock_format.read()
    }

    pub fn document_font(&self) -> String {
        self.document_font.read().clone()
    }
}
