use crate::config::{APP_ID, VERSION};
use glib::object::IsA;
use gtk4::prelude::*;
use gtk4::{Application, License, Window};
use libadwaita::prelude::*;
use libadwaita::AboutWindow;

pub const APP_NAME: &str = "NewsFlash";
pub const COPYRIGHT: &str = "Copyright © 2017-2023 Jan Lukas Gernert";
pub const DESCRIPTION: &str = r#"<b>Follow your favorite blogs &amp; news sites.</b>

NewsFlash is an application designed to complement an already existing web-based RSS reader account.

It combines all the advantages of web based services like syncing across all your devices with everything you expect from a modern desktop application.

<b>Features:</b>

 - Desktop notifications
 - Fast search and filtering
 - Tagging
 - Keyboard shortcuts
 - Offline support
 - and much more

<b>Supported Services:</b>

 - Miniflux
 - local RSS
 - fever
 - Fresh RSS
 - NewsBlur
 - Inoreader
 - feedbin
 - Nextcloud News"#;
pub const WEBSITE: &str = "https://gitlab.com/news-flash/news_flash_gtk";
pub const ISSUE_TRACKER: &str = "https://gitlab.com/news-flash/news_flash_gtk/-/issues";
pub const AUTHORS: &[&str] = &["Jan Lukas Gernert", "Felix Bühler", "Alistair Francis"];
pub const DESIGNERS: &[&str] = &["Jan Lukas Gernert"];
pub const ARTISTS: &[&str] = &["Tobias Bernard"];

pub const RELEASE_NOTES: &str = r#"
<ul>
    <li>Massive update to the internal article scraper</li>
    <li>Remove Mercury Parser</li>
    <li>Use Background Apps Portal</li>
    <li>Update to stable Gtk 4 WebKit API</li>
    <li>Article View: adjust dark background color to match adwaita</li>
    <li>Local RSS: remove scripts in content</li>
    <li>Nextcloud News: fix creating API Url</li>
</ul>
"#;

#[derive(Clone, Debug)]
pub struct NewsFlashAbout;

impl NewsFlashAbout {
    pub fn show<A: IsA<Application> + AdwApplicationExt, W: IsA<Window> + GtkWindowExt>(app: &A, window: &W) {
        let about_window = AboutWindow::builder()
            .application(app)
            .application_name(APP_NAME)
            .transient_for(window)
            .application_icon(APP_ID)
            .developer_name(AUTHORS[0])
            .developers(AUTHORS)
            .designers(DESIGNERS)
            .artists(ARTISTS)
            .license_type(License::Gpl30)
            .version(VERSION)
            .website(WEBSITE)
            .issue_url(ISSUE_TRACKER)
            .comments(DESCRIPTION)
            .copyright(COPYRIGHT)
            .release_notes(RELEASE_NOTES)
            .release_notes_version("2.3")
            .build();
        about_window.show();
    }
}
