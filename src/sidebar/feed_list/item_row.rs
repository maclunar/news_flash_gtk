use crate::app::App;
use crate::sidebar::feed_list::models::{FeedListItemGObject, FeedListItemID};
use crate::util::{GtkUtil, Util};
use futures::channel::oneshot;
use futures::future::FutureExt;
use gio::{Menu, MenuItem};
use glib::{clone, subclass::*, ParamSpec, ParamSpecString, ParamSpecUInt, SignalHandlerId, Value};
use gtk4::TreeListRow;
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, GestureClick, GestureLongPress, Image, Label,
    PopoverMenu, PositionType, TreeExpander, Widget,
};
use log::warn;
use news_flash::models::{CategoryID, FavIcon, PluginCapabilities};
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use std::cell::Cell;
use std::str;
use std::sync::Arc;

static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
    vec![Signal::builder("activated")
        .param_types([ItemRow::static_type()])
        .build()]
});

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/feedlist_item.ui")]
    pub struct ItemRow {
        #[template_child]
        pub expander: TemplateChild<TreeExpander>,
        #[template_child]
        pub row_click: TemplateChild<GestureClick>,
        #[template_child]
        pub row_activate: TemplateChild<GestureClick>,
        #[template_child]
        pub row_long_press: TemplateChild<GestureLongPress>,
        #[template_child]
        pub item_count_label: TemplateChild<Label>,
        #[template_child]
        pub title: TemplateChild<Label>,
        #[template_child]
        pub favicon: TemplateChild<Image>,

        pub popover: Arc<RwLock<Option<PopoverMenu>>>,
        pub right_click_source: RwLock<Option<SignalHandlerId>>,
        pub long_press_source: RwLock<Option<SignalHandlerId>>,
        pub id: RwLock<FeedListItemID>,
        pub parent_id: RwLock<CategoryID>,
        pub label: RwLock<Arc<String>>,
        pub item_count: Cell<u32>,
        pub sort_index: Cell<i32>,
    }

    impl Default for ItemRow {
        fn default() -> Self {
            Self {
                expander: TemplateChild::default(),
                row_click: TemplateChild::default(),
                row_activate: TemplateChild::default(),
                row_long_press: TemplateChild::default(),
                item_count_label: TemplateChild::default(),
                title: TemplateChild::default(),
                favicon: TemplateChild::default(),

                popover: Arc::new(RwLock::new(None)),
                right_click_source: RwLock::new(None),
                long_press_source: RwLock::new(None),
                id: RwLock::new(FeedListItemID::Category(CategoryID::new(""))),
                parent_id: RwLock::new(CategoryID::new("")),
                label: RwLock::new(Arc::new("".into())),
                item_count: Cell::new(0),
                sort_index: Cell::new(0),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ItemRow {
        const NAME: &'static str = "ItemRow";
        type ParentType = Box;
        type Type = super::ItemRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ItemRow {
        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecUInt::builder("item-count").build(),
                    ParamSpecString::builder("label").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "item-count" => {
                    let input = value.get().expect("The value needs to be of type `u32`.");
                    self.item_count.set(input);
                    self.obj().update_item_count(input);
                }
                "label" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.obj().update_title(&input);
                    *self.label.write() = Arc::new(input);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "item-count" => self.item_count.get().to_value(),
                "label" => (**self.label.read()).to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for ItemRow {}

    impl BoxImpl for ItemRow {}
}

glib::wrapper! {
    pub struct ItemRow(ObjectSubclass<imp::ItemRow>)
        @extends Widget, Box;
}

impl Default for ItemRow {
    fn default() -> Self {
        let row = glib::Object::new::<Self>();
        row.init();
        row
    }
}

impl ItemRow {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        imp.row_activate.connect_released(clone!(
            @weak self as this => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            this.activate();
        }));
    }

    pub fn bind_model(&self, model: &FeedListItemGObject, list_row: &TreeListRow) {
        let imp = self.imp();
        let is_same_item = *imp.id.read() == model.id();

        let features = App::default().features();
        let support_mutation = features.read().contains(PluginCapabilities::ADD_REMOVE_FEEDS)
            && features.read().contains(PluginCapabilities::MODIFY_CATEGORIES);

        *imp.id.write() = model.id();
        *imp.parent_id.write() = model.parent_id();
        *imp.label.write() = model.label();
        imp.item_count.set(model.item_count());
        imp.sort_index.set(model.sort_index());

        imp.expander.set_list_row(Some(list_row));

        if support_mutation && imp.right_click_source.read().is_none() {
            self.setup_right_click();
        }

        if !is_same_item {
            self.update_item_count(model.item_count());
            self.update_title(model.label().as_str());
            self.update_favicon(Some(model.id()));
        }
    }

    fn setup_right_click(&self) {
        let imp = self.imp();

        imp.right_click_source
            .write()
            .replace(imp.row_click.connect_released(clone!(
                @weak self as this => @default-panic, move |_gesture, times, _x, _y|
            {
                if times != 1 {
                    return
                }

                if App::default().content_page_state().read().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.read().is_none() {
                    imp.popover.write().replace(this.setup_context_popover());
                }

                if let Some(popover) = imp.popover.read().as_ref() {
                    popover.popup();
                };
            })));

        imp.long_press_source
            .write()
            .replace(imp.row_long_press.connect_pressed(clone!(
                @weak self as this => @default-panic, move |_gesture, _x, _y|
            {
                if App::default().content_page_state().read().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.read().is_none() {
                    imp.popover.write().replace(this.setup_context_popover());
                }

                if let Some(popover) = imp.popover.read().as_ref() {
                    popover.popup();
                };
            })));
    }

    fn setup_context_popover(&self) -> PopoverMenu {
        let imp = self.imp();

        let model = Menu::new();
        let edit_item = MenuItem::new(Some("Edit"), None);
        let delete_item = MenuItem::new(Some("Delete"), None);

        match &*imp.id.read() {
            FeedListItemID::Feed(feed_id, feed_mapping) => {
                let tuple_variant = (
                    feed_id.as_str().to_string(),
                    feed_mapping.category_id.as_str().to_string(),
                )
                    .to_variant();
                edit_item.set_action_and_target_value(Some("win.edit-feed-dialog"), Some(&tuple_variant));

                let tuple_variant = (feed_id.as_str().to_string(), imp.label.read().to_string()).to_variant();
                delete_item.set_action_and_target_value(Some("win.enqueue-delete-feed"), Some(&tuple_variant));
            }
            FeedListItemID::Category(category_id) => {
                let variant = category_id.as_str().to_variant();
                edit_item.set_action_and_target_value(Some("win.edit-category-dialog"), Some(&variant));

                let tuple_variant = (category_id.as_str().to_string(), imp.label.read().to_string()).to_variant();
                delete_item.set_action_and_target_value(Some("win.enqueue-delete-category"), Some(&tuple_variant));
            }
        }

        model.append_item(&edit_item);
        model.append_item(&delete_item);

        let popover = PopoverMenu::from_model(Some(&model));
        popover.set_parent(self);
        popover.set_position(PositionType::Bottom);
        popover
    }

    fn update_item_count(&self, count: u32) {
        let imp = self.imp();

        if count > 0 {
            imp.item_count_label.set_label(&count.to_string());
            imp.item_count_label.set_visible(true);
        } else {
            imp.item_count_label.set_visible(false);
        }
    }

    fn update_favicon(&self, feed_id: Option<FeedListItemID>) {
        let imp = self.imp();

        if let Some(FeedListItemID::Feed(feed_id, _parent_id)) = feed_id {
            let (sender, receiver) = oneshot::channel::<Option<FavIcon>>();
            App::default().load_favicon(feed_id, sender);

            let scale = GtkUtil::get_scale(self);
            let this = self.clone();
            let glib_future = receiver.map(move |res| match res {
                Ok(Some(icon)) => {
                    if let Some(data) = &icon.data {
                        let res = GtkUtil::create_texture_from_bytes(data, 16, 16, scale);
                        if let Ok(texture) = res {
                            this.imp().favicon.set_from_paintable(Some(&texture));
                        } else if let Err(error) = res {
                            log::debug!("Favicon '{}': {}", icon.feed_id, error);
                        }
                    }
                }
                Ok(None) => {
                    warn!("Favicon does not contain image data.");
                }
                Err(_) => warn!("Receiving favicon failed."),
            });

            Util::glib_spawn_future(glib_future);
        } else {
            imp.favicon.set_visible(false);
        }
    }

    fn update_title(&self, title: &str) {
        let imp = self.imp();
        imp.title.set_label(title);
    }

    fn activate(&self) {
        self.emit_by_name::<()>("activated", &[&self.clone()]);
    }
}
