mod tag;
mod tag_gobject;

use crate::sidebar::SidebarIterateItem;
use diffus::{edit::Edit, Diffable};
use eyre::{eyre, Result};
use news_flash::models::{Tag, TagID};
use std::collections::HashSet;
pub use tag::TagListTagModel;
pub use tag_gobject::TagGObject;

#[derive(Clone, Default, Debug)]
pub struct TagListModel {
    models: Vec<TagListTagModel>,
    tags: HashSet<TagID>,
}

impl TagListModel {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn is_empty(&self) -> bool {
        self.tags.is_empty()
    }

    pub fn add(&mut self, tag: &Tag) -> Result<()> {
        if self.tags.contains(&tag.tag_id) {
            return Err(eyre!("tag model '{}' already exists in list", tag.tag_id));
        }
        let model = TagListTagModel::new(tag);
        self.tags.insert(model.id.clone());
        self.models.push(model);
        Ok(())
    }

    pub fn generate_diff<'a>(&'a self, other: &'a TagListModel) -> Edit<'_, Vec<TagListTagModel>> {
        self.models.diff(&other.models)
    }

    pub fn sort(&mut self) {
        self.models.sort()
    }

    pub fn calculate_next_item(&self, selected_id: &TagID) -> SidebarIterateItem {
        match self.models.iter().position(|item| &item.id == selected_id) {
            None => SidebarIterateItem::SelectAll,
            Some(pos) => match self.models.get(pos + 1) {
                None => SidebarIterateItem::SelectAll,
                Some(item) => SidebarIterateItem::SelectTagList(item.id.clone()),
            },
        }
    }

    pub fn calculate_prev_item(&self, selected_id: &TagID) -> SidebarIterateItem {
        match self.models.iter().position(|item| &item.id == selected_id) {
            None => SidebarIterateItem::FeedListSelectLastItem,
            Some(pos) => {
                if pos == 0 {
                    SidebarIterateItem::FeedListSelectLastItem
                } else {
                    match self.models.get(pos - 1) {
                        None => SidebarIterateItem::SelectAll,
                        Some(item) => SidebarIterateItem::SelectTagList(item.id.clone()),
                    }
                }
            }
        }
    }

    pub fn first(&self) -> Option<TagListTagModel> {
        self.models.first().cloned()
    }

    pub fn last(&self) -> Option<TagListTagModel> {
        self.models.last().cloned()
    }
}
