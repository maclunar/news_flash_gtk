using Gtk 4.0;
using Adw 1;

template $ArticleViewColumn : Box {
  width-request: 300;
  hexpand: true;
  orientation: vertical;

  Adw.HeaderBar headerbar {
    width-request: 300;
    hexpand: true;
    show-start-title-buttons: false;
    show-end-title-buttons: true;

    [title]
    Box {
      visible: false;
    }

    [end]
    Stack more_actions_stack {
      tooltip-text: _("Article Menu");

      StackPage {
        name: "button";
        title: _("button");
        child: MenuButton more_actions_button {
          valign: center;
          receives-default: true;
          icon-name: "view-more-symbolic";
        };
      }

      StackPage {
        name: "spinner";
        title: _("spinner");
        child: Spinner {
          valign: center;
          halign: center;
          spinning: true;
        };
      }
    }

    Button back_button {
      visible: false;
      receives-default: true;

      Image {
        icon-name: "go-previous-symbolic";
      }
    }

    ToggleButton mark_article_button {
      sensitive: false;
      receives-default: true;
      tooltip-text: _("Toggle Starred");

      Stack mark_article_stack {
        transition-duration: 50;
        transition-type: crossfade;

        StackPage {
          name: "unmarked";
          title: _("unmarked");
          child: Image {
            icon-name: "unmarked-symbolic";
          };
        }

        StackPage {
          name: "marked";
          title: _("marked");
          child: Image {
            icon-name: "marked-symbolic";
          };
        }
      }

      styles [
        "flat",
      ]
    }

    ToggleButton mark_article_read_button {
      sensitive: false;
      receives-default: true;
      tooltip-text: _("Toggle Read");

      Stack mark_article_read_stack {
        transition-duration: 50;
        transition-type: crossfade;

        StackPage {
          name: "read";
          title: _("read");
          child: Image {
            icon-name: "read-symbolic";
          };
        }

        StackPage {
          name: "unread";
          title: _("unread");
          child: Image {
            icon-name: "unread-symbolic";
          };
        }
      }

      styles [
        "flat",
      ]
    }

    [end]
    Adw.Squeezer header_squeezer {
      transition-type: crossfade;
      transition-duration: 50;
      allow-none: true;

      Box header_squeezer_box {
        spacing: 5;

        [start]
        Stack scrap_content_stack {
          transition-duration: 50;
          transition-type: crossfade;

          StackPage {
            name: "button";
            title: _("button");
            child: ToggleButton scrap_content_button {
              sensitive: false;
              receives-default: true;
              valign: center;
              icon-name: "accessories-dictionary-symbolic";
              tooltip-text: _("Try to show full content");
            };
          }

          StackPage {
            name: "spinner";
            title: _("spinner");
            child: Spinner {
              valign: center;
              halign: center;
              spinning: true;
            };
          }
        }

        [end]
        MenuButton tag_button {
          sensitive: false;
          receives-default: true;
          valign: center;
          icon-name: "tag-symbolic";
          tooltip-text: _("Tag Article");

          GestureClick tag_button_click {}
        }

        MenuButton enclosure_button {
          visible: false;
          receives-default: true;
          valign: center;
          icon-name: "mail-attachment-symbolic";
          tooltip-text: _("Enclosures");
        }

        MenuButton share_button {
          receives-default: true;
          valign: center;
          icon-name: "emblem-shared-symbolic";
          tooltip-text: _("Share To");
        }
      }
    }
  }

  $ArticleView article_view {}

  Revealer footer_revealer {
    transition-type: slide_up;
    child: ActionBar {
      height-request: 47;

      [end]
      Box {
        spacing: 5;

        styles [
          "article-footer",
        ]

        Stack footer_scrap_content_stack {
          transition-duration: 50;
          transition-type: crossfade;

          StackPage {
            name: "button";
            title: _("button");
            child: ToggleButton footer_scrap_content_button {
              sensitive: false;
              receives-default: true;
              icon-name: "accessories-dictionary-symbolic";
              tooltip-text: _("Try to show full content");
            };
          }

          StackPage {
            name: "spinner";
            title: _("spinner");
            child: Spinner {
              valign: center;
              halign: center;
              spinning: true;
            };
          }
        }

        MenuButton footer_tag_button {
          sensitive: false;
          receives-default: true;
          icon-name: "tag-symbolic";
          tooltip-text: _("Tag Article");

          GestureClick footer_tag_button_click {}
        }

        MenuButton footer_enclosure_button {
          visible: false;
          receives-default: true;
          icon-name: "mail-attachment-symbolic";
          tooltip-text: _("Enclosures");
        }

        MenuButton footer_share_button {
          receives-default: true;
          icon-name: "emblem-shared-symbolic";
          tooltip-text: _("Share To");
        }
      }
    };
  }
}
